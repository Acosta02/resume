//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BaseBall.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class Team
    {
        public Team()
        {
            this.Players = new HashSet<Player>();
        }
    
        public int TeamID { get; set; }

        [Required]
        [StringLength(60)]
        public string TeamName { get; set; }

        [Required]
        [StringLength(60)]
        public string ManagerName { get; set; }
    
        public virtual ICollection<Player> Players { get; set; }
    }
}
