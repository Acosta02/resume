﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using SGBank.Data;
using SGBank.Models;

namespace SGBank.BLL
{
    public class AccountManager
    {
        #region Interfaces
        private IAccountRepository _repo;

        public AccountManager()
        {
            _repo = new AccountRepository();
        }

        public AccountManager(IAccountRepository repo)
        {
            _repo = repo;
        }
        #endregion

        #region Get Account
        public Response<Account> GetAccount(int accountNumber)
        {
            
            var response = new Response<Account>();

            try
            {
                var account = _repo.LoadAccount(accountNumber);

                if (account == null)
                {
                    response.Success = false;
                    response.Message = "Account was not found!";
                }
                else
                {
                    response.Success = true;
                    response.Data = account;
                }
            }
            catch (Exception ex)
            {
                // log the exception
                response.Success = false;
                response.Message = "There was an error.  Please try again later.";
            }

            return response;
        }
        #endregion

        #region Deposit Receipt
        public Response<DepositReciept> Deposit(Account account, decimal amount)
        {
            
            var response = new Response<DepositReciept>();

            try
            {
                if (amount <= 0)
                {
                    response.Success = false;
                    response.Message = "Must deposit a positive value.";
                }
                else
                {
                    account.Balance += amount;
                    _repo.UpdateAccount(account);
                    response.Success = true;

                    response.Data = new DepositReciept();
                    response.Data.AccountNumber = account.AccountNumber;
                    response.Data.DepositAmount = amount;
                    response.Data.NewBalance = account.Balance;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }
        #endregion

        #region Withdraw Receipt
        public Response<WithdrawReceipt> Withdraw(Account account, decimal amount)
        {

            var response = new Response<WithdrawReceipt>();

            try
            {
                if (amount <= 0)
                {
                    response.Success = false;
                    response.Message = "Must deposit a positive value.";
                }

                else if(account.Balance < amount)
                {
                    response.Success = false;
                    response.Message = "Not enough funds in account.";
                }

                else
                {
                    account.Balance -= amount;
                    
                    _repo.UpdateAccount(account);
                    response.Success = true;

                    response.Data = new WithdrawReceipt();
                    response.Data.AccountNumber = account.AccountNumber;
                    response.Data.WithdrawAmount = amount;
                    response.Data.NewBalance = account.Balance;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }
        #endregion

        #region Transfer Receipt
        public Response<TransferReceipt> Transfer(Account account, Account accountTwo, decimal amount)
        {

            var response = new Response<TransferReceipt>();

            try
            {
                if (amount <= 0)
                {
                    response.Success = false;
                    response.Message = "Must deposit a positive value.";
                }

                else if (account.Balance < amount)
                {
                    response.Success = false;
                    response.Message = "Not enough funds in account.";
                }

                else
                {
                    account.Balance -= amount;
                    _repo.UpdateAccount(account);
                    accountTwo.Balance += amount;
                    _repo.UpdateAccount(accountTwo);
                    response.Success = true;

                    response.Data = new TransferReceipt();
                    response.Data.AccountNumber = account.AccountNumber;
                    response.Data.TransferAmount = amount;
                    response.Data.NewBalance = account.Balance;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }
        #endregion
        
        #region Create Account
        public Response<Account> CreateAccount(string FirstName, string LastName)
        {
           
            
            var accounts = _repo.GetAllAccounts();

            List<int> AcctNumbers = new List<int>();

            foreach (var a in accounts)
            {
                AcctNumbers.Add(a.AccountNumber);
            }

            AcctNumbers.Sort();
            int NewAcctNumber = 0;

            for (int i = 1; i < AcctNumbers.Count + 1; i++)
            {
                if (i == AcctNumbers.ElementAt(i - 1))
                {
                    continue;
                }

                else
                    NewAcctNumber = i;
            }
            if (NewAcctNumber == 0)
            {
                NewAcctNumber = AcctNumbers.Count + 1;
            }
            Account NewAccount = new Account();
            NewAccount.FirstName = FirstName;
            NewAccount.LastName = LastName;
            NewAccount.AccountNumber = NewAcctNumber;
            NewAccount.Balance = 0;
            Response<Account> finalAccount = _repo.AddNewAccount(NewAccount);
            
            return
                finalAccount;
        }
        #endregion

        #region Delete Account
        public Response<Account> DeleteAccount(Account DelAccount)
        {
           
            var response = new Response<Account>();

           
                var account = _repo.GetAllAccounts();

                if (account == null)
                {
                    response.Success = false;
                    response.Message = "Invalid Account!";
                }
                else
                {
                    response.Success = true;
                    response.Message = "Account has been deleted.";
                    _repo.DeleteAccount(DelAccount);
                    return response;
                }
           
                response.Success = false;
                response.Message = "There was an error.  Please try again later.";
           
            return response;
        }
        #endregion


    }
}
