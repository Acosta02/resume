﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Models;
using System.IO;

namespace SGBank.Data
{
    public class AccountRepository : IAccountRepository
    {
        private const string FilePath = @"DataFiles\Bank.txt";

        public List<Account> GetAllAccounts()
        {
            List<Account> accounts = new List<Account>();

            var reader = File.ReadAllLines(FilePath);

            for (int i = 1; i < reader.Length; i++)
            {
                var columns = reader[i].Split(',');

                var account = new Account();

                account.AccountNumber = int.Parse(columns[0]);
                account.FirstName = columns[1];
                account.LastName = columns[2];
                account.Balance = decimal.Parse(columns[3]);

                accounts.Add(account);
            }

            return accounts;
        }

        public Account LoadAccount(int accountNumber)
        {
            List<Account> accounts = GetAllAccounts();
            return accounts.FirstOrDefault(a => a.AccountNumber == accountNumber);
        }

        public void UpdateAccount(Account accountToUpdate)
        {
            var accounts = GetAllAccounts();

            var existingAccount = accounts.First(a => a.AccountNumber == accountToUpdate.AccountNumber);
            existingAccount.FirstName = accountToUpdate.FirstName;
            existingAccount.LastName = accountToUpdate.LastName;
            existingAccount.Balance = accountToUpdate.Balance;

            OverwriteFile(accounts);
        }

        public Response<Account> AddNewAccount(Account NewAccount)
        {

            using (StreamWriter sw = File.AppendText(FilePath))
            {
                sw.WriteLine("{0},{1},{2},{3}", NewAccount.AccountNumber, NewAccount.FirstName, NewAccount.LastName, NewAccount.Balance);
                var response = new Response<Account>();
                response.Data = NewAccount;
                response.Success = true;
                response.Message = "New Account is created";
                return response;
            }

        }

        public void DeleteAccount(Account DeleteAcct)
        {

            var accounts = this.GetAllAccounts();
            List <Account> clone = new List<Account>();
            for (int i = 0; i < accounts.Count; i++)
            {
                clone.Add(accounts.ElementAt(i));
            }
            foreach (var account in clone)
            {
                if (DeleteAcct.AccountNumber == account.AccountNumber)
                {
                    accounts.Remove(account);
                }
            }
            OverwriteFile(accounts);
   
        }





        private void OverwriteFile(List<Account> accounts)
        {
            File.Delete(FilePath);

            using (var writer = File.CreateText(FilePath))
            {
                writer.WriteLine("AccountNumber,FirstName,LastName,Balance");

                foreach (var account in accounts)

                    writer.WriteLine("{0},{1},{2},{3}", account.AccountNumber, account.FirstName, account.LastName, account.Balance);
            }
        }

    }
}

