﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Models;

namespace SGBank.Data
{
    public class TestRepo : IAccountRepository
    {
        public Response<Account> AddNewAccount(Account NewAccount)
        {
            Response<Account> response = new Response<Account>()
            {
                Success = true,
                Message = "Account Created",
                Data = new Account()
                {
                    AccountNumber = 5,
                    FirstName = "FirstName",
                    LastName = "LastName",
                    Balance = 0
                }

        };
            return response;
        }

        public void DeleteAccount(Account DeleteAcct)
        {
            throw new NotImplementedException();
        }

        public List<Account> GetAllAccounts()
        {
            throw new NotImplementedException();
        }

        public Account LoadAccount(int accountNumber)
        {
            throw new NotImplementedException();
        }

        public void UpdateAccount(Account accountToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
