﻿using System.Collections.Generic;
using SGBank.Models;

namespace SGBank.Data
{
    public interface IAccountRepository
    {
        Response<Account> AddNewAccount(Account NewAccount);
        void DeleteAccount(Account DeleteAcct);
        List<Account> GetAllAccounts();
        Account LoadAccount(int accountNumber);
        void UpdateAccount(Account accountToUpdate);
    }
}