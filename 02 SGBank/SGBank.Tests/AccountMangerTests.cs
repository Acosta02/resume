﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGBank.BLL;
using SGBank.Data;
using SGBank.Models;

namespace SGBank.Tests
{
    [TestFixture]
    public class AccountMangerTests
    {
        [Test]
        public void FoundAccountReturnsSuccess()
        {
            var manager = new AccountManager();

            var response = manager.GetAccount(1);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(1, response.Data.AccountNumber);
            Assert.AreEqual("Mary", response.Data.FirstName);
        }

        [Test]
        public void NotFoundAccountReturnsFail()
        {
            var manager = new AccountManager();

            var response = manager.GetAccount(9999);

            Assert.IsFalse(response.Success);
        }


        [Test]
        public void DeleteAccountReturnsSuccess()
        {
            var manager = new AccountManager();

            var findAccount = manager.GetAccount(2);

            var response = manager.DeleteAccount(findAccount.Data);

            Assert.IsTrue(response.Success);
        }


        [Test]
        public void CreateAccountReturnsSuccess()
        {
            IAccountRepository repo = new TestRepo();
            var manager = new AccountManager(repo);
            var response = manager.CreateAccount("FirstName", "LastName");
            Assert.IsTrue(response.Success);
            Assert.AreEqual(5, response.Data.AccountNumber);
            Assert.AreEqual("FirstName", response.Data.FirstName);
        }


    }
}
