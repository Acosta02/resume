﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Models;
using SGBank.BLL;


namespace SGBank.UI.Workflows
{
    public class CreateWorkflow
    {
        public void Execute()
        {
            Console.WriteLine("Please enter account holder's first name: ");
            string FirstName = Console.ReadLine();

            Console.WriteLine("Please enter account holder's last name: ");
            string LastName = Console.ReadLine();

            AccountManager manager = new AccountManager();
            Response<Account> newAccount = manager.CreateAccount(FirstName, LastName);

            Console.WriteLine(newAccount.Message);
            Console.WriteLine(string.Format("{0} {1}, {2}.", newAccount.Data.AccountNumber, newAccount.Data.LastName, newAccount.Data.FirstName));
            Console.WriteLine("Press any key to continue");
            Console.ReadLine();
       
        }

    }
}
