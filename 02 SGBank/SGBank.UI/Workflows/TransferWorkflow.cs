﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.Models;

namespace SGBank.UI.Workflows
{
    public class TransferWorkflow
    {
        public void Execute(Account account)
        {
            var manager = new AccountManager();
            var withdraw = new WithdrawWorkflow();

            LookupWorkflow GetAcctNumber = new LookupWorkflow();
            int acctNumber = GetAcctNumber.GetAccountNumberFromUser();
            var accountTwo = manager.GetAccount(acctNumber);
            GetAcctNumber.PrintAccountDetails(accountTwo.Data);

            decimal amount = withdraw.GetWithdrawAmount();

            var response = manager.Transfer(account, accountTwo.Data, amount);

            

            if (response.Success)
            {
                Console.Clear();
                Console.WriteLine("Transfer amount {0:c} from account {1} to account {2}. New balance for account {2}, is {3:C}", amount, account.AccountNumber, accountTwo.Data.AccountNumber, accountTwo.Data.Balance);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
                Console.Clear();
                Response < Account > accountResponse = new Response<Account>();
                accountResponse.Data = account;
                accountResponse.Success = true;
                GetAcctNumber.PrintAccountDetails(accountResponse.Data);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("An error occurred.  {0}", response.Message);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }

        }






    }
}
