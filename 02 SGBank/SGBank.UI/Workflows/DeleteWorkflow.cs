﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Models;
using SGBank.BLL;

namespace SGBank.UI.Workflows
{
    class DeleteWorkflow
    {
        public void Execute()
        {

            LookupWorkflow GetAcctNumber = new LookupWorkflow();
            int acctNumber = GetAcctNumber.GetAccountNumberFromUser();

            AccountManager manager = new AccountManager();
            var response = manager.GetAccount(acctNumber);
            string input;
            do
            {

                Console.Clear();
                Console.WriteLine("{0}, {1}, {2}, {3}", response.Data.AccountNumber, response.Data.FirstName, response.Data.LastName, response.Data.Balance);
                Console.WriteLine("Are you sure you want to delete this account? Y/N");
                input = Console.ReadLine().ToUpper();

            if (input == "Y")
            {
                var deleteResponse = manager.DeleteAccount(response.Data);
                Console.WriteLine(deleteResponse.Message);
                Console.ReadLine();

            }

            } while (input != "Y" && input != "N");

        }
    }
}
