﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Conditionals
    {
        /* We have two children, a and b, and the parameters aSmile and 
           bSmile indicate if each is smiling. We are in trouble if they 
           are both smiling or if neither of them is smiling. Return true 
           if we are in trouble. 
        */
        public bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            if (aSmile && bSmile)
                return true;

            if (!aSmile && !bSmile)
                return true;

            return false;
        }

        public bool SleepIn(bool isWeekday, bool isVacation)
        {
            if (!isWeekday)
                return true;
            if (isVacation)
                return true;
            return false;
        }

        public int SumDouble(int a, int b)
        {
            if (a == b)
                return (a+ b) * 2;
            return a + b;
        }

        public int Diff21(int n)
        {
            int answer;
            if (n <= 21)
                answer = Math.Abs(21 - n);
            else
                answer = 2 * (n - 21);
            return answer;
        }

        public bool ParrotTrouble(bool isTalking, int hour)
        {
            if (isTalking && (hour < 7 || hour > 20))
                return true;
            return false;

        }

        public bool Makes10(int a, int b)
        {
            if (a == 10 || b == 10 || a + b == 10)
                return true;
            return false;
        }

        public bool NearHundred(int n)
        {
            bool near100 = false;
            bool near200 = false;
            if (Math.Abs(100 - n) <= 10)
                near100 = true;
            if (Math.Abs(200 - n) <= 10)
                near200 = true;
            if ((near100 == true) || (near200 == true))
                return true;
            return false;
        }

        public bool PosNeg(int a, int b, bool negative)
        {
            if (negative == true)
                if (a < 0 && b < 0)
                    return true;
                else
                    return false;
            else
            {
                if (a < 0 && b < 0)
                    return false;
                else if (a > 0 && b > 0)
                    return false;
                else
                    return true;
            }
        }

        public string NotString(string str)
        {
            if (str.Length < 3)
                return string.Format("not {0}", str);
            string notCheck = str.Substring(0, 3);
            if (notCheck == "not")
                return str;
            else
                return string.Format("not {0}", str);
        }

        public string MissingChar(string str, int n)
        {
            string a = str.Substring(0, n);
            string b = str.Substring(n + 1);
            return string.Concat(a, b);
        }

        public string FrontBack(string str)
        {
            if (str.Length < 2)
                return str;
            string a = str.Substring(0, 1);
            string b = str.Substring(1, str.Length - 2);
            string c = str.Substring(str.Length-1);
            return string.Format("{0}{1}{2}", c, b, a);
        }

        public string Front3(string str)
        {
            string just3 = "";
            if (str.Length < 3)
                just3 = str;
            else
                just3 = str.Substring(0, 3);
            return string.Format("{0}{0}{0}", just3);
        }

        public string BackAround(string str)
        {
            string lastChar = str.Substring(str.Length - 1);
            return lastChar + str + lastChar;
        }

        public bool Multiple(int n)
        {
            if (n % 3 == 0 || n % 5 == 0)
                return true;
            return false;
        }

        public bool HiFirst(string str)
        {
            if (str.Length < 3)
            {
                if (str == "hi")
                    return true;
                else
                    return false;
            }
            else
            {
                string hiCheck = str.Substring(0, 3);
                if (hiCheck == "hi ")
                    return true;
                else
                    return false;
            }
        }

        public bool IcyHot(int temp1, int temp2)
        {
            bool icy = false;
            bool hot = false;
            if (temp1 < 0 || temp2 < 0)
                icy = true;
            if (temp1 > 100 || temp2 > 100)
                hot = true;
            if (icy && hot)
                return true;
            return false;
        }
            
        public bool Between(int a, int b)
        {
            bool oneIn = false;
            if (a >= 10 && a <= 20)
                oneIn = true;
            if (b >= 10 && b <= 20)
                oneIn = true;
            return oneIn;
        }

        public bool HasTeen(int a, int b, int c)
        {
            //I could use a list and a for loop here to simplify this
            bool oneIn = false;
            if (a >= 13 && a <= 19)
                oneIn = true;
            if (b >= 13 && b <= 19)
                oneIn = true;
            if (c >= 13 && c <= 19)
                oneIn = true;
            return oneIn;
        }

        public bool SoAlone(int a, int b)
        {
            bool teenA = false;
            bool teenB = false;
            if (a >= 13 && a <= 19)
                teenA = true;
            if (b >= 13 && b <= 19)
                teenB = true;
            if (teenA && teenB)
                return false;
            if (!teenA && !teenB)
                return false;
            return true;
        }

        public string RemoveDel(string str)
        {
            string delCheck = str.Substring(1, 3);
            if (delCheck == "del")
            {
                string let1 = str.Substring(0, 1);
                string notDel = str.Substring(4);
                return let1 + notDel;
            }
            return str;
        }

        public bool IxStart(string str)
        {
            string ixCheck = str.Substring(1, 2);
            if (ixCheck == "ix")
                return true;
            return false;
        }

        public string StartOz(string str)
        {
            string oYes = "";
            string zYes = "";
            string oCheck = str.Substring(0, 1);
            string zCheck = str.Substring(1, 1);
            if (oCheck == "o")
                oYes = "o";
            if (zCheck == "z")
                zYes = "z";
            return string.Format("{0}{1}", oYes, zYes);
        }

        public int Max(int a, int b, int c)
        {
            int highNum = 0;
            highNum = a;
            if (b > a)
                highNum = b;
            if (c > highNum)
                highNum = c;
            return highNum;
        }

        public int Closer(int a, int b)
        {
            int absA = Math.Abs(10 - a);
            int absB = Math.Abs(10 - b);
            if (absA == absB)
                return 0;
            else if (absA < absB)
                return a;
            else
                return b;
        }

        public bool GetE(string str)
        {
            int startLength = str.Length;
            int endLength = str.Replace("e","").Length;
            if (startLength - endLength <= 3)
                return true;
            return false;
        }

        public string EndUp(string str)
        {
            if (str.Length <= 3)
                return str.ToUpper();
            int fromEnd = str.Length - 3;
            string start = str.Substring(0, fromEnd);
            string toUp = str.Substring(fromEnd);
            string upper = toUp.ToUpper();
            return start + upper;
        }

        public string EveryNth(string str, int n)
        {
            string nthTaken = "";
            for (int i = 0; i < str.Length; i+=n)
            {
                nthTaken += str.Substring(i, 1);
            }
            return nthTaken;
        }



    }
}
