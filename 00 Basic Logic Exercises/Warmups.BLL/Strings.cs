﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Strings
    {
        // Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!". 
        public string SayHi(string name)
        {
            return string.Format("Hello {0}!", name);
        }

        public string Abba(string a, string b)
        {
            return string.Format("{0}{1}{1}{0}", a, b);
        }

        public string Tags(string tag, string content)
        {
            return string.Format("<{0}>{1}</{0}>", tag, content);
        }

        public string InsertWord(string container, string word)
        {
            string start = container.Substring(0, 2);
            string end = container.Substring(2, 2);
            return string.Format("{0}{1}{2}", start, word, end);
        }

        public string MultipleEndings(string str)
        {
            int startpoint = str.Length - 2;
            string end = str.Substring(startpoint, 2);
            return string.Format("{0}{0}{0}", end);
        }

        public string FirstHalf(string str)
        {
            int halfvalue = str.Length / 2;
            string firstHalf = str.Substring(0, halfvalue);
            return firstHalf;
        }

        public string TrimOne(string str)
        {
            string trimmed = str.Substring(1, str.Length - 2);
            return trimmed;
        }

        public string LongInMiddle(string a, string b)
        {
            string longWord = "";
            string shortWord = "";

            if (a.Length > b.Length)
            {
                longWord = a;
                shortWord = b;
            }
            else
            {
                shortWord = a;
                longWord = b;
            }

            return string.Format("{0}{1}{0}", shortWord, longWord, shortWord);
        }

        public string RotateLeft2(string str)
        {
            string car = str.Substring(0, 2);
            string train = str.Substring(2);
            return string.Format("{0}{1}", train, car);
        }

        public string RotateRight2(string str)
        {
            int minusTwo = str.Length - 2;
            string car = str.Substring(minusTwo);
            string train = str.Substring(0, minusTwo);
            return string.Format("{0}{1}", car, train);
        }

        public string TakeOne(string str, bool fromFront)
        {
            int minusOne = str.Length - 1;

            if (fromFront)
                return str.Substring(0, 1);
            else
                return str.Substring(minusOne);
        }

        public string MiddleTwo(string str)
        {
            int firstLetNum = (str.Length / 2) - 1;
            return str.Substring(firstLetNum, 2);
        }

        public bool EndsWithLy(string str)
        {
            if (str.Length < 2)
                return false;
            int num = str.Length - 2;
            string lastTwo = str.Substring(num);
            return lastTwo == "ly";
        }

        public string FrontAndBack(string str, int n)
        {
            int fromBack = str.Length - n;
            string front = str.Substring(0, n);
            string back = str.Substring(fromBack);
            return string.Format("{0}{1}", front, back);
        }

        public string TakeTwo(string str, int n)
        {
            if (n + 2 > 4)
                return str.Substring(0, 2);
            else
                return str.Substring(n, 2);
        }

        public bool HasBad(string str)
        {
            if (str.Length < 3)
                return false;
            string firstThree = str.Substring(0, 3);
            string nextThree = str.Substring(1, 3);
            if (firstThree == "bad" || nextThree == "bad")
                return true;
            else
                return false;
        }

        public string AtFirst(string str)
        {
            if (str.Length < 2)
            {
                string longer = str + "@@";
                string shorter = longer.Substring(0, 2);
                return shorter;
            }
            string firstTwo = str.Substring(0, 2);
            return firstTwo;
        }

        public string FirstLast(string a, string b)
        {
            string firstA = "";
            string lastB = "";
            int positB = b.Length - 1;

            if (a == "")
                firstA = "@";
            else
                firstA = a.Substring(0, 1);

            if (b == "")
                lastB = "@";
            else
                lastB = b.Substring(positB);

            return string.Concat(firstA, lastB);
        }

        public string ConCatClass(string a, string b)
        {
            if (a == "")
                return string.Concat(a, b);
            if (b == "")
                return string.Concat(a, b);
            int positA = a.Length - 1;
            string lastA = a.Substring(positA);
            if (b.Substring(0, 1) == lastA)
            {
                string chopB = b.Substring(1);
                return string.Concat(a, chopB);
            }
            else
                return string.Concat(a, b);
        }

        public string SwapLast(string str)
        {
            int num = str.Length - 1;
            string lastLet = str.Substring(num, 1);
            str = str.Remove(num);
            string answer = str.Insert(num - 1, lastLet); ;
            return answer;
        }

        public bool FrontAgain(string str)
        {
            int twoFrom = str.Length - 2;
            string front = str.Substring(0, 2);
            string back = str.Substring(twoFrom);
            bool check = front == back;
            return check;
        }

        public string MinCat(string a, string b)
        {
            int lengthA = a.Length;
            int lengthB = b.Length;
            int lengthShort;
            if (lengthA > lengthB)
                lengthShort = lengthB;
            else
                lengthShort = lengthA;
            int startHereA = lengthA - lengthShort;
            int startHereB = lengthB - lengthShort;
            string shortA = a.Substring(startHereA);
            string shortB = b.Substring(startHereB);
            string answer = string.Concat(shortA, shortB);
            return answer;
        }

        public string TweakFront(string str)
        {
            string newStr = str.Substring(2);
            string checkA = str.Substring(0, 1);
            string checkB = str.Substring(1, 1);
            if (checkB == "b")
                newStr = newStr.Insert(0, "b");
            if (checkA == "a")
                newStr = newStr.Insert(0, "a");
            return newStr;
        }

        public string StripX(string str)
        {
            str = str.Trim('x');
            return str;
        }











    }
}
