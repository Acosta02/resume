﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Logic
    {
        /* When squirrels get together for a party, they like to have cigars. 
           A squirrel party is successful when the number of cigars is between 
           40 and 60, inclusive. Unless it is the weekend, in which case there is 
           no upper bound on the number of cigars. Return true if the party with 
           the given values is successful, or false otherwise. 
        */
        public bool GreatParty(int cigars, bool isWeekend)
        {
            if (isWeekend)
                return cigars > 40;
            else
                return (cigars >= 40 && cigars <= 60);
        }

        public int HazTable(int yourStyle, int dateStyle)
        {
            if (yourStyle <= 2 || dateStyle <= 2)
                return 0;
            else if (yourStyle >= 8 || dateStyle >= 8)
                return 2;
            else
                return 1;
        }

        public bool PlayOutside(int temp, bool isSummer)
        {
            int tempLimit = 0;
            if (isSummer)
                tempLimit = 100;
            if (!isSummer)
                tempLimit = 90;
            return temp < tempLimit;
        }

        public int CaughtSpeeding(int speed, bool isBirthday)
        {
            int lowLimit = 60;
            int highLimit = 80;
            if (isBirthday)
            {
                lowLimit += 5;
                highLimit += 5;
            }
            if (speed <= lowLimit)
                return 0;
            else if (speed <= highLimit)
                return 1;
            else
                return 2;
                
        }

        public int SkipSum(int a, int b)
        {
            int sum = a + b;
            if (10 <= sum && sum <= 19)
                return 20;
            return sum;
        }

        public string AlarmClock(int day, bool vacation)
        {
            bool isWeekend = false;
            int time = 0;
            if (day == 0 || day == 6)
                isWeekend = true;
            if (isWeekend && vacation)
                return "off";
            else if ((isWeekend && !vacation) || (!isWeekend && vacation))
                time = 10;
            else
                time = 7;
            return string.Format("{0}:00", time);

        }

        public bool LoveSix(int a, int b)
        {
            if (a == 6 || b == 6)
                return true;
            if (a + b == 6)
                return true;
            return false;
        }

        public bool InRange(int n, bool outsideMode)
        {
            bool inRange = false;
            if (n >= 1 && n <= 10)
                inRange = true;
            if (inRange == outsideMode)
                return false;
            return true;
        }

        public bool Eleven(int n)
        {
            if (n % 11 == 0 || n % 11 == 1)
                return true;
            return false;
        }

        public bool Twenty(int n)
        {
            if (n % 20 == 1 || n % 20 == 2)
                return true;
            return false;
        }

        public bool ThirtyFive(int n)
        {
            bool mod3 = false;
            bool mod5 = false;
            if (n % 3 == 0)
                mod3 = true;
            if (n % 5 == 0)
                mod5 = true;
            if (mod3 == mod5)
                return false;
            return true;
        }

        public bool AnswerCell(bool isMorning, bool isMom, bool isAsleep)
        {
            if (isAsleep)
                return false;
            if (isMorning && !isMom)
                return false;
            return true;
        }

        public bool TwoIsOne(int a, int b, int c)
        {
            int high = Math.Max(a, b);
            int highest = Math.Max(high, c);
            if (a + b == highest || a + c == highest || b + c == highest)
                return true;
            return false;
        }

        public bool InOrder(int a, int b, int c, bool bOk)
        {
            if(bOk)
            {
                if (c > b && c > a)
                    return true;
                return false;
            }
            else
            {
                if (c > b && b > a)
                    return true;
                return false;
            }
        }

        public bool OrderEqual(int a, int b, int c, bool equalOK)
        {
            if (equalOK)
            {
                if (c >= b && b >= a)
                    return true;
                return false;
            }
            else
            {
                if (c > b && b > a)
                    return true;
                return false;
            }
                
        }

        public bool LastDigit(int a, int b, int c)
        {
            int lastA = a % 10;
            int lastB = b % 10;
            int lastC = c % 10;
            if (lastA == lastB || lastA == lastC || lastB == lastC)
                return true;
            return false;

        }

        public int RollDice(int dieA, int dieB, bool noDoubles)
        {
            int total = dieA + dieB;
            if (noDoubles && dieA == dieB)
            {
                if (total == 12)
                    return 11;
                else
                    return total + 1;
            }
            else return total;
        }


    }
}
