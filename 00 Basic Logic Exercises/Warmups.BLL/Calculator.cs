﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Calculator
    {
        public int Add(string numbers)
        {
            string numbersCleaned = numbers.Replace("\n",",");
            var numArray = numbersCleaned.Split(',');
            var result = 0;

            if (numbers == "")
                return 0;
            if (numbers.Length == 1)
                return int.Parse(numbers);
            if (numArray.Length > 1)
            { 
                foreach (string s in numArray)
                    result += int.Parse(s);
                return result;
            }
            return int.MaxValue;
        }
    }
}
