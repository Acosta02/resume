﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Loops
    {
        /* Given a string and a non-negative int n, return a 
           larger string that is n copies of the original string. 
        */
        public string StringTimes(string str, int n)
        {
            string result = "";

            for (int i = 1; i <= n; i++)
            {
                result += str;
            }

            return result;
        }

        public string FrontTimes(string str, int n)
        {
            string result = "";
            if (str.Length <= 3)
            {
                for (int i = 1; i <= n; i++)
                {
                    result += str;
                }
            }
            else
            {
                string front = str.Substring(0, 3);
                for (int i = 1; i <= n; i++)
                {
                    result += front;
                }
            }
            return result;
        }

        public int CountXX(string str)
        {
            int xFound = 0;
            for (int i = 0; i < str.Length-1; i++)
            {
                string xCheck = str.Substring(i, 2);
                if (xCheck == "xx")
                    xFound += 1;
            }
            return xFound;
        }

        public bool DoubleX(string str)
        {
            for (int i = 0; i < str.Length-1; i++)
            {
                string xCheck = str.Substring(i, 1);
                string xxCheck = str.Substring(i, 2);
                if (xCheck == "x")
                {
                    if (xxCheck == "xx")
                        return true;
                    else
                        return false;
                }
            }
            return false;
        }

        public string EveryOther(string str)
        {
            string final = "";
            for (int i = 0; i < str.Length; i+=2)
            {
                string letter = str.Substring(i, 1);
                final += letter;
            }
            return final;
        }

        public string Splosion(string str)
        {
            string sploded = "";
            for (int i = 1; i <= str.Length; i++)
            {
                string splode = str.Substring(0, i);
                sploded += splode;
            }
            return sploded;
        }

        public int Last2(string str)
        {
            string last2 = str.Substring(str.Length - 2);
            string iCheck = "";
            int total = 0;
            for (int i = 0; i < str.Length-2; i++)
            {
                iCheck = str.Substring(i, 2);
                if (iCheck == last2)
                    total += 1;
            }
            return total;
        }

        public int Nines(int[] numbers)
        {
            int total = 0;
            foreach (int n in numbers)
            {
                if (n == 9)
                    total++;
            }
            return total;
        }

        public bool FrontNine(int[] numbers)
        {
            int maxCheck = 4;
            if (numbers.Length < maxCheck)
                maxCheck = numbers.Length;
            for (int i = 0; i <= maxCheck-1; i++)
            {
                if (numbers.ElementAt(i) == 9)
                    return true;
            }
            return false;
        }

        public bool Array123(int[] numbers)
        {
            for (int i = 0; i < numbers.Length-2; i++)
            {
                if (numbers.ElementAt(i) == 1)
                {
                    if (numbers.ElementAt(i+1) == 2)
                    {
                        if (numbers.ElementAt(i + 2) == 3)
                            return true;
                    }
                    continue;
                }
                continue;
            }
            return false;
        }

        public int SubStringMatch(string a, string b)
        {
            int maxLength = Math.Min(a.Length, b.Length);
            int total = 0;
            for (int i = 0; i < maxLength-1; i++)
            {
                if (a.Substring(i, 2) == b.Substring(i, 2))
                    total++;
            }
            return total;
        }

        public string StringX(string str)
        {
            string strShort = str;
            int countFor = str.Length - 1;
            for (int i = 1; i < countFor; i++)
            {
                if (str.ElementAt(i) == 'x')
                {
                    str = str.Remove(i, 1);
                    countFor = countFor - 1;
                    i--;
                }

            }
            return str;
            
        }

        public string AltPairs(string str)
        {
            string strPairs = "";
            for (int i = 0; i <= str.Length-1; i+=4)
            {
                if (i == str.Length - 1)
                    strPairs += str.Substring(i);
                else
                    strPairs += str.Substring(i, 2);
            }
            return strPairs;
        }

        public string NoYak (string str)
        {
            int maxCount = str.Length - 2;
            for (int i = 0; i < maxCount; i++)
            {
                if (str.ElementAt(i) == 'y')
                {
                    if (str.ElementAt(i + 2) == 'k')
                    {
                        str = str.Remove(i, 3);
                        maxCount -= 3;
                    }
                }
            }
            return str;
        }

        public int Array667(int[] numbers)
        {
            int total = 0;
            for (int i = 0; i < numbers.Length-1; i++)
            {
                if (numbers.ElementAt(i) == 6)
                {
                    if (numbers.ElementAt(i + 1) == 6 || numbers.ElementAt(i + 1) == 7)
                        total += 1;
                }
            }
            return total;
        }

        public bool NoTriples(int[] numbers)
        {
            for (int i = 0; i < numbers.Length-3; i++)
            {
                if (numbers.ElementAt(i) == numbers.ElementAt(i + 1) &&
                    numbers.ElementAt(i) == numbers.ElementAt(i + 2))
                    return false;
            }
            return true;
        }

        public bool Pattern271(int[] numbers)
        {
            for (int i = 0; i < numbers.Length-2; i++)
            {
                if (numbers.ElementAt(i) + 5 == numbers.ElementAt(i + 1))
                    if (numbers.ElementAt(i) - 1 == numbers.ElementAt(i + 2))
                        return true;
            }
            return false;
        }










    }
}
