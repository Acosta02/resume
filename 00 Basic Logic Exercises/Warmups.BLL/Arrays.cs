﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Arrays
    {
        /* Given an array of ints, return true if 6 appears 
           as either the first or last element in the array. 
           The array will be length 1 or more. 
        */
        public bool FirstLast6(int[] numbers)
        {
            // 0 is always the first index and 
            // Length - 1 of an array is always the last index
            return (numbers[0] == 6 || numbers[numbers.Length - 1] == 6);
        }

        public bool SameFirstLast(int[] numbers)
        {
            if (numbers.Length >= 1 &&
                numbers[0] == numbers[numbers.Length - 1])
                return true;
            return false;
        }

        //public int[] MakePi(int n)
        //{
        //    int[] piStore = new int[n];
        //    var pi = Math.PI;
        //    for (int i = 0; i < n; i++)
        //    {
        //        piStore[i] = 
        //    }
        //}
        
        public bool CommonEnd(int[] a, int[] b)
        {
            int a1 = a[0];
            int b1 = b[0];
            int aEnd = a[a.Length - 1];
            int bEnd = b[b.Length - 1];
            if (a1 == b1 || aEnd == bEnd)
                return true;
            return false;
        }

        public int Sum(int[] numbers)
        {
            int total = 0;

            foreach (var i in numbers)
            {
                total += i;
            }

            return total;
        }

        public int[] RotateLeft(int[] numbers)
        {
            int firstNumber = numbers.ElementAt(0);
            for (int i = 0; i < numbers.Length-1; i++)
            {
                numbers[i] = numbers[i + 1];
            }
            numbers[numbers.Length - 1] = firstNumber;
            return numbers;
        }
        
        public int[] Reverse(int[] numbers)
        {
            int[] newArray = new int[numbers.Length];
            for (int i = 0; i < numbers.Length; i++)
            {
                newArray[i] = numbers[(numbers.Length - 1) - i];
            }
            return newArray;

        }

        public int[] HigherWins(int[] numbers)
        {
            int highNum = 0;
            if (numbers.First() > numbers.Last())
                highNum = numbers.First();
            else
                highNum = numbers.Last();
            
            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = highNum;
            }
            return numbers;
        }

        public int[] GetMiddle(int[] a, int[] b)
        {
            int middleIndexA = (a.Length - 1) / 2;
            int middleIndexB = (b.Length - 1) / 2;
            int[] middleArray = new int[2];
            middleArray[0] = a[middleIndexA];
            middleArray[1] = b[middleIndexB];
            return middleArray;
        }

        public bool HasEven(int[] numbers)
        {
            bool hasEven = false;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] % 2 == 0)
                    hasEven = true;
            }
            return hasEven;
        }

        public int[] KeepLast(int[] numbers)
        {
            int[] emptyArray = new int[(numbers.Length * 2)];
            emptyArray[emptyArray.Length - 1] = numbers[numbers.Length - 1];
            return emptyArray;
        }

        public bool Double23(int[] numbers)
        {
            int twoCount = 0;
            int threeCount = 0;
            foreach (var n in numbers)
            {
                if (n == 2)
                    twoCount++;
                if (n == 3)
                    threeCount++;
            }
            if (twoCount == 2 || threeCount == 2)
                return true;
            else return false;
        }

        public int[] Fix23(int[] numbers)
        {
            for (int i = 0; i < numbers.Length-1; i++)
            {
                if (numbers[i] == 2)
                    if (numbers[i + 1] == 3)
                        numbers[i + 1] = 0;
            }
            return numbers;
        }

        public bool Unlucky1s(int[] numbers)
        {
            for (int i = 0; i < 2; i++)
            {
                if (numbers[i] == 1)
                    if (numbers[i + 1] == 3)
                        return true;
            }
            if (numbers[numbers.Length-2] == 1)
                if (numbers[numbers.Length - 1] == 3)
                    return true;
            return false;
        }

        public int[] Make2(int[]a, int[] b)
        {
            List<int> emptyList = new List<int>();
            int[] twoArray = new int[2];
            for (int i = 0; i < a.Length; i++)
            {
                emptyList.Add(a[i]);
            }

            for (int i = 0; i < b.Length; i++)
            {
                emptyList.Add(b[i]);
            }

            for (int i = 0; i < 2; i++)
            {
                twoArray[i] = emptyList[i];
            }
            return twoArray;
        }

    }
}
