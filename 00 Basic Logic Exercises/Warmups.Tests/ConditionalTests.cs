﻿using NUnit.Framework;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class ConditionalTests
    {
        [TestCase(true, true, true)]
        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        public void AreWeInTroubleTest(bool aSmile, bool bSmile, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.AreWeInTrouble(aSmile, bSmile);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(false, false,true)]
        [TestCase(true, false,false)]
        [TestCase(false, true,true)]
        public void SleepInTest(bool isWeekday, bool isVacation, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.SleepIn(isWeekday, isVacation);
            Assert.AreEqual(actual, expected);
        }


        [TestCase(1, 2, 3)]
        [TestCase(3, 2, 5)]
        [TestCase(2, 2, 8)]
        public void SumDoubleTest(int a, int b, int expected)
        {
            Conditionals obj = new Conditionals();
            int actual = obj.SumDouble(a, b);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(23, 4)]
        [TestCase(10, 11)]
        [TestCase(21, 0)]
        public void Diff21Test(int n, int expected)
        {
            Conditionals obj = new Conditionals();
            int actual = obj.Diff21(n);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(true, 6, true)]
        [TestCase(true, 7, false)]
        [TestCase(false, 6, false)]
        public void ParrotTest(bool isTalking, int hour, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.ParrotTrouble(isTalking, hour);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(9, 10, true)]
        [TestCase(9, 9, false)]
        [TestCase(1, 9, true)]
        public void Makes10Test(int a, int b, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.Makes10(a, b);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(103, true)]
        [TestCase(90, true)]
        [TestCase(89, false)]
        public void NearHundredTest(int n, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.NearHundred(n);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(1, -1, false, true)]
        [TestCase(-1, 1, false, true)]
        [TestCase(-4, -5, true, true)]
        public void PosNegTest(int a, int b, bool negative, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.PosNeg(a, b, negative);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("candy", "not candy")]
        [TestCase("x", "not x")]
        [TestCase("not bad", "not bad")]
        public void NotTest(string str, string expected)
        {
            Conditionals obj = new Conditionals();
            string actual = obj.NotString(str);
            Assert.AreEqual(actual, expected);

        }

        [TestCase("kitten", 1, "ktten")]
        [TestCase("kitten", 0, "itten")]
        [TestCase("kitten", 4, "kittn")]
        public void MissingTest(string str, int n, string expected)
        {
            Conditionals obj = new Conditionals();
            string actual = obj.MissingChar(str, n);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("code", "eodc")]
        [TestCase("a", "a")]
        [TestCase("ab", "ba")]
        public void FBackTest(string str, string expected)
        {
            Conditionals obj = new Conditionals();
            string actual = obj.FrontBack(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("Microsoft", "MicMicMic")]
        [TestCase("Chocolate", "ChoChoCho")]
        [TestCase("at", "atatat")]
        public void Front3Test(string str, string expected)
        {
            Conditionals obj = new Conditionals();
            string actual = obj.Front3(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("cat", "tcatt")]
        [TestCase("Hello", "oHelloo")]
        [TestCase("a", "aaa")]
        public void BackAroundTest(string str, string expected)
        {
            Conditionals obj = new Conditionals();
            string actual = obj.BackAround(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(3, true)]
        [TestCase(10, true)]
        [TestCase(8, false)]
        public void MultTest(int n, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.Multiple(n);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("hi there", true)]
        [TestCase("hi", true)]
        [TestCase("high up", false)]
        public void HiFirstTest(string str, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.HiFirst(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(120, -1, true)]
        [TestCase(-1, 120, true)]
        [TestCase(2, 120, false)]
        public void IcyHotTest(int temp1, int temp2, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.IcyHot(temp1, temp2);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(12, 99, true)]
        [TestCase(21, 12, true)]
        [TestCase(8, 99, false)]
        public void BetweenTest(int a, int b, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.Between(a, b);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(13, 20, 10, true)]
        [TestCase(20, 19, 10, true)]
        [TestCase(20, 10, 12, false)]
        public void TeenTest(int a, int b, int c, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.HasTeen(a, b, c);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(13, 99, true)]
        [TestCase(21, 19, true)]
        [TestCase(13, 13, false)]
        public void AloneTest(int a, int b, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.SoAlone(a, b);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("adelbc", "abc")]
        [TestCase("adelHello", "aHello")]
        [TestCase("adedbc", "adedbc")]
        public void DelTest(string str, string expected)
        {
            Conditionals obj = new Conditionals();
            string actual = obj.RemoveDel(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("mix snacks", true)]
        [TestCase("pix snacks", true)]
        [TestCase("piz snacks", false)]
        public void IxTest(string str, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.IxStart(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("ozymandias", "oz")]
        [TestCase("bzoo", "z")]
        [TestCase("oxx", "o")]
        public void OzTest(string str, string expected)
        {
            Conditionals obj = new Conditionals();
            string actual = obj.StartOz(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(1, 2, 3, 3)]
        [TestCase(1, 3, 2, 3)]
        [TestCase(3, 2, 1, 3)]
        public void MaxTest(int a, int b, int c, int expected)
        {
            Conditionals obj = new Conditionals();
            int actual = obj.Max(a, b, c);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(8, 13, 8)]
        [TestCase(13, 8, 8)]
        [TestCase(13, 7, 0)]
        public void CloserTest(int a, int b, int expected)
        {
            Conditionals obj = new Conditionals();
            int actual = obj.Closer(a, b);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("Hello", true)]
        [TestCase("Heelle", true)]
        [TestCase("Heelele", false)]
        public void ETest(string str, bool expected)
        {
            Conditionals obj = new Conditionals();
            bool actual = obj.GetE(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("Hello", "HeLLO")]
        [TestCase("hi there", "hi thERE")]
        [TestCase("hi", "HI")]
        public void EndUpTest(string str, string expected)
        {
            Conditionals obj = new Conditionals();
            string actual = obj.EndUp(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("Miracle", 2, "Mrce")]
        [TestCase("abcdefg", 2, "aceg")]
        [TestCase("abcdefg", 3, "adg")]
        public void NthTest(string str, int n, string expected)
        {
            Conditionals obj = new Conditionals();
            string actual = obj.EveryNth(str, n);
            Assert.AreEqual(actual, expected);
        }







    }
}
