﻿using NUnit.Framework;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class StringTests
    {
        [TestCase("Bob", "Hello Bob!")]
        [TestCase("Alice", "Hello Alice!")]
        [TestCase("X", "Hello X!")]
        public void SayHiTest(string name, string expected)
        {
            // arrange
            Strings obj = new Strings();

            // act
            string actual = obj.SayHi(name);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hi","Bye","HiByeByeHi")]
        [TestCase("Yo","Alice","YoAliceAliceYo")]
        [TestCase("What","Up","WhatUpUpWhat")]
        public void AbbaTest(string a, string b, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.Abba(a, b);

            Assert.AreEqual(actual, expected);
        }

        [TestCase("i","Yay","<i>Yay</i>")]
        [TestCase("i","Hello","<i>Hello</i>")]
        [TestCase("cite","Yay","<cite>Yay</cite>")]
        public void TagsTest(string tag, string content, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.Tags(tag, content);

            Assert.AreEqual(actual, expected);
        }

        [TestCase("<<>>", "Yay","<<Yay>>")]
        [TestCase("<<>>", "WooHoo","<<WooHoo>>")]
        [TestCase("[[]]", "word","[[word]]")]
        public void InsertTest(string container, string word, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.InsertWord(container, word);

            Assert.AreEqual(actual, expected);
        }

        [TestCase("Hello","lololo")]
        [TestCase("ab","ababab")]
        [TestCase("Hi","HiHiHi")]
        public void EndingTest(string str, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.MultipleEndings(str);

            Assert.AreEqual(actual, expected);
        }


        [TestCase("WooHoo","Woo")]
        [TestCase("HelloThere","Hello")]
        [TestCase("abcdef","abc")]
        public void HalfTest(string str, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.FirstHalf(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("Hello","ell")]
        [TestCase("java","av")]
        [TestCase("coding","odin")]
        public void trimTest(string str, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.TrimOne(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("Hello", "hi","hiHellohi")]
        [TestCase("hi", "Hello","hiHellohi")]
        [TestCase("aaa", "b","baaab")]
        public void longerTest(string a, string b, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.LongInMiddle(a, b);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("Hello","lloHe")]
        [TestCase("java","vaja")]
        [TestCase("Hi","Hi")]
        public void RotLeftTest(string str, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.RotateLeft2(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("Hello","loHel")]
        [TestCase("java","vaja")]
        [TestCase("Hi","Hi")]
        public void RotRightTest(string str, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.RotateRight2(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("Hello", true,"H")]
        [TestCase("Hello", false,"o")]
        [TestCase("oh", true,"o")]
        public void TakeOneTest(string str, bool fromFront, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.TakeOne(str, fromFront);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("string","ri")]
        [TestCase("code","od")]
        [TestCase("Practice","ct")]
        public void MiddleTest(string str, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.MiddleTwo(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("oddly",true)]
        [TestCase("y",false)]
        [TestCase("oddy",false)]
        public void EndsWithLyTest(string str, bool expected)
        {
            Strings obj = new Strings();
            bool actual = obj.EndsWithLy(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("Hello", 2,"Helo")]
        [TestCase("Chocolate", 3,"Choate")]
        [TestCase("Chocolate", 1,"Ce")]
        public void FrontBackTest(string str, int n, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.FrontAndBack(str, n);
            Assert.AreEqual(actual, expected);
        }
        
        [TestCase("java", 0,"ja")]
        [TestCase("java", 2,"va")]
        [TestCase("java", 3,"ja")]
        public void TakeTwoTest(string str, int n, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.TakeTwo(str, n);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("badxx",true)]
        [TestCase("xbadxx",true)]
        [TestCase("xxbadxx",false)]
        public void HasBadTest(string str, bool expected)
        {
            Strings obj = new Strings();
            bool actual = obj.HasBad(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("hello","he")]
        [TestCase("hi","hi")]
        [TestCase("h","h@")]
        public void AtFirstTest(string str, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.AtFirst(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("last", "chars","ls")]
        [TestCase("yo", "mama","ya")]
        [TestCase("hi", "","h@")]
        public void FirstLastTest(string a, string b, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.FirstLast(a, b);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("abc", "cat","abcat")]
        [TestCase("dog", "cat","dogcat")]
        [TestCase("abc", "","abc")]
        public void ConCatTest(string a, string b, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.ConCatClass(a, b);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("coding","codign")]
        [TestCase("cat","cta")]
        [TestCase("ab","ba")]
        public void SwapLastTest(string str, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.SwapLast(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("edited",true)]
        [TestCase("edit",false)]
        [TestCase("ed",true)]
        public void FrontAgainTest(string str, bool expected)
        {
            Strings obj = new Strings();
            bool actual = obj.FrontAgain(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("Hello", "Hi","loHi")]
        [TestCase("Hello", "java","ellojava")]
        [TestCase("java", "Hello","javaello")]
        public void MinCatTest(string a, string b, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.MinCat(a, b);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("Hello","llo")]
        [TestCase("away","aay")]
        [TestCase("abed","abed")]
        public void TweakTest(string str, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.TweakFront(str);
            Assert.AreEqual(actual, expected);
        }

        [TestCase("xHix","Hi")]
        [TestCase("xHi","Hi")]
        [TestCase("Hxix","Hxi")]
        public void StripTest(string str, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.StripX(str);
            Assert.AreEqual(actual, expected);
        }


    }
}
