﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRproject.Models
{
    public class Policy
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Category { get; set; }
    }
}