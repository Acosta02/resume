﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HRproject.Models
{
    public class PoliciesViewModel
    {
        public List<Policy> Policies;
        public List<SelectListItem> Categories;
    }
}