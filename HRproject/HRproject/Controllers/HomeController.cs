﻿using HRproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace HRproject.Controllers
{
    public class HomeController : Controller
    {

        public static List<Policy> PoliciesMaster = new List<Policy>()
        {
            new Policy
            {
                Name = "No Running",
                Text = "No running in the hallways!",
                Category = "Workplace Behavior"
            },
            new Policy
            {
                Name = "No spitting",
                Text = "Absolutely no spitting.",
                Category = "Workplace Behavior"
            }
        };

        public static List<string> CategoriesMaster = new List<string>()
        {
            "Workplace Behavior"
        };
        

        PoliciesViewModel policiesView = new PoliciesViewModel();


        public ActionResult Index()
        {
            policiesView.Policies = PoliciesMaster;
            policiesView.Categories = GetCategoryList();

            return View(policiesView);
        }

        public ActionResult ManagePolicies()
        {
            policiesView.Policies = PoliciesMaster;
            

            return View(policiesView);
        }

        public ActionResult Apply()
        {
            return View();
        }

        public ActionResult ManageCategories()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            foreach (var c in CategoriesMaster)
            {
                SelectListItem s = new SelectListItem()
                {
                    Text = c,
                    Value = c
                };
                listItems.Add(s);
            }

            policiesView.Categories = listItems;

            return View(policiesView);
        }

        [HttpPost]
        public ActionResult CreateCategory(string Name)
        {
            CategoriesMaster.Add(Name);

            List<SelectListItem> listItems = new List<SelectListItem>();
            foreach (var c in CategoriesMaster)
            {
                SelectListItem s = new SelectListItem()
                {
                    Text = c,
                    Value = c
                };
                listItems.Add(s);
            }

            policiesView.Categories = listItems;
            policiesView.Policies = PoliciesMaster;
            return View("ManageCategories", policiesView);
        }


        public ActionResult AddPolicy()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            foreach (var c in CategoriesMaster)
            {
                SelectListItem s = new SelectListItem()
                {
                    Text = c,
                    Value = c
                };
                listItems.Add(s);
            }

            policiesView.Categories = listItems;
            policiesView.Policies = PoliciesMaster;

            return View(policiesView);
        }

        [HttpPost]
        public ActionResult CreatePolicy(string Name, string Text, string Category)
        {
            Policy newPol = new Policy()
            {
                Name = Name,
                Text = Text,
                Category = Category
            };
            PoliciesMaster.Add(newPol);

            return RedirectToAction("ManagePolicies", policiesView);
        }

        [HttpPost]
        public ActionResult DeletePolicy(string Name)
        {
            foreach (var policy in PoliciesMaster)
            {
                if(policy.Name == Name)
                {
                    PoliciesMaster.Remove(policy);
                    break;
                }
            }
            policiesView.Policies = PoliciesMaster;

            return View("ManagePolicies",policiesView);

        }

        private List<SelectListItem> GetCategoryList()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            foreach (var c in CategoriesMaster)
            {
                SelectListItem s = new SelectListItem()
                {
                    Text = c,
                    Value = c
                };
                listItems.Add(s);
            }

            return listItems;
        }
    }
}