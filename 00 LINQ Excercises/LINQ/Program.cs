﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ
{
    class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */
        static void Main()
        {
            ListOfInStockGrouped();

            Console.ReadLine();
        }

        //1
        private static void PrintOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock == 0);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }
        //2
        private static void PrintOver3()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock > 0 && p.UnitPrice > 3);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }
        //3
        private static void PrintWashington()
        {
            var customers = DataLoader.LoadCustomers();

            var results = customers.Where(c => c.Region == "WA");

            foreach (var customer in results)
            {
                Console.WriteLine(">>>>>>>>>>>" + customer.CompanyName);
                foreach (var o in customer.Orders)
                {
                    Console.WriteLine(o.OrderID);
                    Console.WriteLine(o.OrderDate);
                    Console.WriteLine(o.Total);
                    Console.WriteLine(" ");
                }
            }
        }
        //4
        private static void ProductList()
        {
            var products = DataLoader.LoadProducts();

            List<string> productlist = new List<string>();

            foreach (var product in products)
            {
                productlist.Add(product.ProductName);
            }

            foreach (string s in productlist)
            {
                Console.WriteLine(s);
            }
        }
        //5
        private static void ListHigherPrices()
        {
            var products = DataLoader.LoadProducts();
            var result = products.Select(x => new { x.ProductName, price = (decimal)1.25 * x.UnitPrice });
            

            foreach (var item in result)
            {
                Console.WriteLine(item.ProductName + ", " + item.price);
            }
        }
        //6
        private static void ListInUpper()
        {
            var products = DataLoader.LoadProducts();
            var result = products.Select(x => x.ProductName.ToUpper());

            foreach (string s in result)
            {
                Console.WriteLine(s);
            };
        }
        //7
        private static void ListEvenNumbers()
        {
            var products = DataLoader.LoadProducts();
            var result = products.Where(x => x.UnitsInStock % 2 == 0);
            foreach (var i in result)
            {
                Console.WriteLine(i.ProductName);
            }
        }
        //8
        private static void Simplify()
        {
            var products = DataLoader.LoadProducts();
            var result = products.Select(x => new { x.ProductName, x.Category, Price = x.UnitPrice });

            foreach (var i in result)
            {
                Console.WriteLine(i.ProductName + ", " + i.Category + ", " + i.Price);
            }
        }
        //9
        //google "linq cross join"
        private static void MatchMaker()
        {
            var numbersB = DataLoader.NumbersB;
            var numbersC = DataLoader.NumbersC;
            var result = from b in numbersB
                         from c in numbersC
                         where b < c
                         select new {b, c};
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
            
        }
        //10
        private static void CheapOrders()
        {
            var customers = DataLoader.LoadCustomers();
            var results = from customer in customers
                          from o in customer.Orders
                          where o.Total < 500
                          select new { customer.CustomerID, o.OrderID, total = o.Total };
            foreach (var i in results)
            {
                Console.WriteLine(i.CustomerID + " " + i.OrderID + " " + i.total);
            }
        }
        //11
        private static void JustThree()
        {
            var numbersA = DataLoader.NumbersA;
            var firstThree = numbersA.Take(3);

            foreach (var item in firstThree)
            {
                Console.WriteLine(item);
            }                     
        }
        //12

        private static void ThreeOrders()
        {
            var customers = DataLoader.LoadCustomers();
            var results = (from customer in customers
                           where customer.Region == "WA"
                           from o in customer.Orders.Take(3)
                           select new { customer.CompanyName, o });

            foreach (var i in results)
            {
                Console.WriteLine(i.CompanyName);
                Console.WriteLine(i.o.OrderID + " " + i.o.OrderDate + " " + i.o.Total);
            }
        }
        //13
        private static void SkipThree()
        {
            var numA = DataLoader.NumbersA;
            var result = numA.Skip(3);

            foreach (var i in result)
            {
                Console.Write(i);
            }
        }
        //14
        private static void SkipTwoOrders()
        {
            var customers = DataLoader.LoadCustomers();
            var results = (from customer in customers
                           where customer.Region == "WA"
                           from o in customer.Orders.Skip(2)
                           select new { customer.CompanyName, o });

            foreach (var i in results)
            {
                Console.WriteLine(i.CompanyName);
                Console.WriteLine(i.o.OrderID + " " + i.o.OrderDate + " " + i.o.Total);
            }
        }
        //15
        //takewhile
        private static void Until6()
        {
            var numC = DataLoader.NumbersC;
            var results = numC.TakeWhile(n => n < 6);
            foreach (var item in results)
            {
                Console.Write(item);
            }
        }

        // Exercise 16
        private static void GreaterThanPosition()
        {
            var numC = DataLoader.NumbersC;
            var results = numC.TakeWhile(x => x >= Array.IndexOf(numC, x));

            foreach (var i in results)
            {
                Console.Write(i);
            }
        }

        //17
        private static void SkipNotDivisible()
        {
            var numC = DataLoader.NumbersC;
            var results = numC.SkipWhile(n => n % 3 != 0);
            foreach (var item in results)
            {
                Console.Write(item);
            }
        }
        //18
        private static void ProductsAlphabetical()
        {
            var products = DataLoader.LoadProducts();
            var result = products.OrderBy(x => x.ProductName);
            foreach (var item in result)
            {
                Console.WriteLine(item.ProductName);
            }
        }
        //19
        private static void ProductsDescending()
        {
            var product = DataLoader.LoadProducts();
            var result = product.OrderByDescending(x => x.UnitsInStock);

            foreach (var item in result)
            {
                Console.WriteLine(item.ProductName + ", " + item.UnitsInStock + " left");
            }
        }
        //20
        private static void TripleSort()
        {
            var products = DataLoader.LoadProducts();
            var results = products.OrderBy(x => x.Category).ThenByDescending(x => x.UnitPrice);

            foreach (var item in results)
            {
                Console.WriteLine(item.Category + " " + item.ProductName + " " + item.UnitPrice);
            }
        }
        //21
        private static void ReverseC()
        {
            var numC = DataLoader.NumbersC;
            var results = numC.Reverse();

            foreach (var item in results)
            {
                Console.WriteLine(item);
            }
        }
        //22
        private static void GroupByRemainder()
        {
            var numC = DataLoader.NumbersC;
            var results = numC.GroupBy(n => n % 5).Select(x => new { Remainder = x.Key, Number = x });

            foreach (var item in results)
            {
                foreach (var n in item.Number)
                {
                    Console.WriteLine(item.Remainder + ", " + n);
                }
            }
        }
        //23
        private static void GroupByCategory()
        {
            var products = DataLoader.LoadProducts();
            var results = products.GroupBy(n => n.Category).Select(x => new { Category = x.Key, Name = x });

            foreach (var item in results)
            {
                foreach (var p in item.Name)
                {
                    Console.WriteLine(item.Category + ", " + p.ProductName);
                }
            }
        }
        
        //24. Group customer orders by year, then by month.
        private static void GroupEm()
        {
            var customers = DataLoader.LoadCustomers();
            var result = from customer in customers
                         from order in customer.Orders
                         group order by order.OrderDate.Year into yr
                         select new
                         {
                             Year = yr.Key,
                             MonthGroups = from yrs in yr
                                           group yrs by yrs.OrderDate.Month into mnths
                                           select new { Month = mnths.Key, Orders = mnths}
                         };

            foreach (var item in result)
            {
                foreach (var m in item.MonthGroups)
                {
                    foreach (var o in m.Orders)
                    {
                        Console.WriteLine(item.Year + " " + m.Month + " " + o.OrderID);
                    }
                }
            }
        }
        //25
        private static void CategoryList()
        {
            var products = DataLoader.LoadProducts();
            var result = products.Select(x => x.Category).Distinct();

            foreach (var i in result)
            {
                Console.WriteLine(i);
            }
        }
        //26
        private static void UniqueValues()
        {
            var numA = DataLoader.NumbersA;
            var numB = DataLoader.NumbersB;
            var result = numA.Concat(numB).Distinct();

            foreach (var n in result)
            {
                Console.WriteLine(n);
            }
        }
        //27
        private static void SharedValues()
        {
            var numA = DataLoader.NumbersA;
            var numB = DataLoader.NumbersB;
            var result = numA.Intersect(numB);

            foreach (var n in result)
            {
                Console.WriteLine(n);
            }
        }
        //28
        private static void DifferentValues()
        {
            var numA = DataLoader.NumbersA;
            var numB = DataLoader.NumbersB;
            var result = numA.Except(numB);

            foreach (var n in result)
            {
                Console.WriteLine(n);
            }
        }
        //29
        private static void First12()
        {
            var products = DataLoader.LoadProducts();
            var result = products.Where(x => x.ProductID == 12).Single();

            Console.WriteLine(result.ProductName + ", " + result.ProductID);
        }
        //30
        private static void LookFor789()
        {
            var products = DataLoader.LoadProducts();
            var results = products.Any(x => x.ProductID == 789);
            Console.WriteLine(results);
        }
        //31
        private static void LookForOutOfStock()
        {
            var products = DataLoader.LoadProducts();
            var result = (from p in products
                         where p.UnitsInStock == 0
                         select p.Category).Distinct();

            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }
        //32
        private static void LessThan9()
        {
            var numB = DataLoader.NumbersB;
            var result = numB.All(x => x < 9);

            Console.WriteLine(result);
        }
        //33
        private static void ListOfInStockGrouped()
        {
            var products = DataLoader.LoadProducts();
            //products.Where(x => x.UnitsInStock ==0_.GroupBy(x=>x.Category);

            //var filtered=products.where(x=>x.UnitsInStock==0).Select(x=>x.Category);
            //var prods = products.where(x=>!filtered.Contains(x.category)).GroupBy(x=>x.Category);

            var categories = products.GroupBy(x => x.Category);

            foreach(var group in categories)
            {
                if(group.All(p=>p.UnitsInStock >= 0))
                {
                    Console.WriteLine("somehow display category name");
                }
            }

            //var results = from p in products
            //              group p by p.Category into newGroup
            //              from x in newGroup
            //              where (x.UnitsInStock == 0)
            //              select newGroup;
            //foreach (var item in results)
            //{
            //    Console.WriteLine(item.Key);
            //    foreach (var p in item)
            //    {
            //        Console.WriteLine(p.Category + " " + p.ProductName + ", " + p.UnitsInStock + " in stock.");
            //    }
            //}             
        }
        //34
        private static void GetOdds()
        {
            var numA = DataLoader.NumbersA;
            var result = numA.Where(n => n % 2 != 0);

            foreach (var n in result)
            {
                Console.Write(n);
            }
        }
        //35
        private static void OrderCount()
        {
            var customers = DataLoader.LoadCustomers();
            var results = from c in customers
                          select new { ID = c.CustomerID, orderNo = c.Orders.Length };

            foreach (var i in results)
            {
                Console.WriteLine("Customer " + i.ID + " has " + i.orderNo + " orders placed.");
            }
        }
        //36
        private static void CountPerCategory()
        {
            var products = DataLoader.LoadProducts();
            var results = from p in products
                          group p by p.Category into newGroup
                          select newGroup;

            foreach (var item in results)
            {
                Console.WriteLine(item.Key + " " + item.Count());
            }
        }
        //37
        private static void CategoryUnits()
        {
            var products = DataLoader.LoadProducts();
            var results = from p in products
                          group p by p.Category into newGroup
                          select new { Category = newGroup.Key, Units = newGroup.Sum(x => x.UnitsInStock) };

            foreach (var item in results)
            {
                Console.WriteLine(item.Category +" " + item.Units);
            }
        }
        //38
        private static void LowestPrice()
        {
            var products = DataLoader.LoadProducts();
            var results = from p in products
                          group p by p.Category into newGroup
                          select new { Category = newGroup.Key, Cheapest = newGroup.Min(x => x.UnitPrice) };

            foreach (var p in results)
            {
                Console.WriteLine(p.Category + " " + p.Cheapest);
            }
        }
        //39
        private static void HighestPrice()
        {
            var products = DataLoader.LoadProducts();
            var results = from p in products
                          group p by p.Category into newGroup
                          select new { Category = newGroup.Key, Pricey = newGroup.Max(x => x.UnitPrice) };

            foreach(var p in results)
            {
                Console.WriteLine(p.Category + " " + p.Pricey);
            }
        }

        //40
        private static void AveragePrice()
        {
            var products = DataLoader.LoadProducts();
            var results = from p in products
                          group p by p.Category into newGroup
                          select new { Category = newGroup.Key, Average = newGroup.Average(x => x.UnitPrice) };

            foreach (var p in results)
            {
                Console.WriteLine(p.Category + " " + p.Average);
            }
        }





    }
}
