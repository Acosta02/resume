﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors;
using NUnit.Framework;
using RPS.Tests.TestCases;

namespace RPS.Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void PaperBeatsRock()
        {
            GamePlay newGame = new GamePlay(new ChoosePaper());
            string str = "";
            newGame.p2Choice = newGame.selector.MakeChoice(str);

            GameResult result = newGame.CompareChoices(RPSChoices.Rock, newGame.p2Choice);
            Assert.AreEqual(GameResult.Player2Win, result);
        }

        [Test]
        public void ScissorsBeatsPaper()
        {
            GamePlay newGame = new GamePlay(new ChoosePaper());
            string str = "";
            newGame.p2Choice = newGame.selector.MakeChoice(str);

            GameResult result = newGame.CompareChoices(RPSChoices.Scissors, newGame.p2Choice);
            Assert.AreEqual(GameResult.Player1Win, result);
        }

        [Test]
        public void RockBeatsScissors()
        {
            GamePlay newGame = new GamePlay(new ChooseRock());
            string str = "";
            newGame.p2Choice = newGame.selector.MakeChoice(str);

            GameResult result = newGame.CompareChoices(RPSChoices.Scissors, newGame.p2Choice);
            Assert.AreEqual(GameResult.Player2Win, result);
        }

    }
}
