﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors;
using RockPaperScissors.PlayerClasses;

namespace RPS.Tests.TestCases
{
    public class ChoosePaper : IChoiceSelector
    {
        public RPSChoices MakeChoice(string str)
        {
            return RPSChoices.Paper;
        }
    }
}
