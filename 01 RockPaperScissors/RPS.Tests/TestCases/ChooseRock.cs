﻿using RockPaperScissors;
using RockPaperScissors.PlayerClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPS.Tests.TestCases
{
    class ChooseRock : IChoiceSelector
    {
        public RPSChoices MakeChoice(string str)
        {
            return RPSChoices.Rock;
        }
    }
}
