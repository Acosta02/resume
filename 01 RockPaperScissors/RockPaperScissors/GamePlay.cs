﻿using RockPaperScissors.PlayerClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public class GamePlay
    {
        public string p1Name;
        public string p2Name;
        public RPSChoices p1Choice = new RPSChoices();
        public RPSChoices p2Choice = new RPSChoices();
        public GameMode currentGameMode;
        public IChoiceSelector selector { get; set; }

        public GamePlay ()
        {

        }

        public GamePlay (IChoiceSelector c)
        {
            selector = c;
        }

        public GameResult GameLoop(Player player1, IChoiceSelector selector)
        {
            p1Choice = player1.MakeChoice(p1Name);

            p2Choice = selector.MakeChoice(p2Name);

            return CompareChoices(p1Choice, p2Choice);
        }

        public GameResult CompareChoices(RPSChoices p1Choice, RPSChoices p2Choice)
        {
            if (p1Choice == RPSChoices.Paper && p2Choice == RPSChoices.Rock)
                return GameResult.Player1Win;
            else if (p1Choice == RPSChoices.Paper && p2Choice == RPSChoices.Scissors)
                return GameResult.Player2Win;
            else if (p1Choice == RPSChoices.Rock && p2Choice == RPSChoices.Paper)
                return GameResult.Player2Win;
            else if (p1Choice == RPSChoices.Rock && p2Choice == RPSChoices.Scissors)
                return GameResult.Player1Win;
            else if (p1Choice == RPSChoices.Scissors && p2Choice == RPSChoices.Paper)
                return GameResult.Player1Win;
            else if (p1Choice == RPSChoices.Scissors && p2Choice == RPSChoices.Rock)
                return GameResult.Player2Win;
            else
                return GameResult.Tie;
        }

        public void EndGame(RPSChoices p1Choice, RPSChoices p2Choice, GameResult gameOutcome)
        {
            string winnerName;

            if (gameOutcome == GameResult.Player1Win)
                winnerName = p1Name;
            else
                winnerName = p2Name;

            Console.WriteLine("{0} chose {1}", p1Name, p1Choice);
            Console.WriteLine("{0} chose {1}", p2Name, p2Choice);

            Console.WriteLine("Congratulations, {0}! You won!", winnerName);
            Console.ReadLine();
        }

        public static bool ValidateChoice(string playerResponse)
        {
            if (playerResponse == "1" || playerResponse == "2" || playerResponse == "3")
                return true;
            else
                Console.WriteLine("I'm sorry, I didn't get that. Enter your choice again.");
            return false;
        }

        public static string WriteMenu()
        {
            Console.WriteLine("Please choose a mode:");
            Console.WriteLine("1. 2 Player Mode");
            Console.WriteLine("2. 1 Player Mode");
            Console.WriteLine("3. VS The Librarian");
            string playerMode = Console.ReadLine();
            while (GamePlay.ValidateChoice(playerMode) == false)
            {
                playerMode = Console.ReadLine();
            }
            return playerMode;
        }

        public static bool ValidateContinue(string playerResponse)
        {
            if (playerResponse.ToLower() == "y" || playerResponse.ToLower() == "n")
                return true;
            else
                Console.WriteLine("I'm sorry, I didn't get that. Enter your choice again.");
            return false;
        }

        public static string Continue()
        {
            Console.WriteLine("Would you like to play another game? (Y/N)");
            string repeatPlay = Console.ReadLine();
            while (GamePlay.ValidateContinue(repeatPlay) == false)
            {
                repeatPlay = Console.ReadLine();
            }
            return repeatPlay;
        }
    }
}
