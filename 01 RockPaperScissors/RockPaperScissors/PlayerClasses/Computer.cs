﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.PlayerClasses;

namespace RockPaperScissors.PlayerClasses
{
    public class Computer : IChoiceSelector
    {
        public int paperWeight;
        public int scissorsWeight;
        public int rockWeight;

        public Computer()
        {
            paperWeight = 1;
            scissorsWeight = 1;
            rockWeight = 1;
        }

        //One item is added to the list(numberPossible) for each point of "weight" in a given RPSChoice. 
        //More Weighted computers can easily be added with desired "weighted random" RPSChoices, no math required!
        public RPSChoices MakeChoice(string playerName)
        {
            List<RPSChoices> numberPossible = new List<RPSChoices>();
            Random r = new Random();

            for (int i = 0; i < paperWeight; i++)
            {
                numberPossible.Add(RPSChoices.Paper);
            }

            for (int i = 0; i < rockWeight; i++)
            {
                numberPossible.Add(RPSChoices.Rock);
            }

            for (int i = 0; i < scissorsWeight; i++)
            {
                numberPossible.Add(RPSChoices.Scissors);
            }

            int randomSelect = r.Next(0, numberPossible.Count);

            RPSChoices playerChoice = numberPossible.ElementAt(randomSelect);

            return playerChoice;
        }
    }
}
