﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.PlayerClasses
{
    public interface IChoiceSelector
    {
        RPSChoices MakeChoice(string playerName);
    }
}
