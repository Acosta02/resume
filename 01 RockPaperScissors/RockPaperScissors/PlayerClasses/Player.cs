﻿using RockPaperScissors.PlayerClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public class Player : IChoiceSelector
    {
     
        public RPSChoices MakeChoice(string playerName)
        {
            Console.WriteLine("{0}, please make your choice.", playerName);
            Console.WriteLine("1. Rock  2. Paper  3. Scissors");
            string playerResponse = Console.ReadLine().Trim();
            while (GamePlay.ValidateChoice(playerResponse) == false)
            {
                playerResponse = Console.ReadLine();
            }

            Console.Clear();

            Console.WriteLine("Thanks, {0}!", playerName);

            RPSChoices playerChoice = (RPSChoices)Enum.Parse(typeof(RPSChoices), playerResponse);

            return playerChoice;
        }

        
    }
}
