﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.PlayerClasses
{
    public class WeightedComputer : Computer
    {
        public WeightedComputer()
        {
            paperWeight = 8;
            scissorsWeight = 2;
            rockWeight = 1;
        }
        
    }
}
