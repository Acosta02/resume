﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public enum GameResult
    {
        Player1Win,
        Player2Win,
        Tie
    }
}
