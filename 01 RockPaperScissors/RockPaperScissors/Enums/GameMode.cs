﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public enum GameMode
    {
        TwoPlayer = 1,
        OnePlayer = 2,
        VsLibrarian = 3
    }
}
