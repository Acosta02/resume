﻿using RockPaperScissors.PlayerClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Welcome to Rock Paper Scissors!");

            //Flag to determine if game resets or not
            bool continueGame = true;

            do
            {
                //Setup - Main Menu
                string playerMode = GamePlay.WriteMenu();

                Console.WriteLine("Player 1, what is your name?");
                string player1name = Console.ReadLine();

                GamePlay playGame = new GamePlay();


                //Setup - Create Second Player
                if (playerMode == "1")
                {
                    playGame = new GamePlay(new Player());
                    Console.WriteLine("Player 2, what is your name?");
                    playGame.p2Name = Console.ReadLine();
                }
                else if (playerMode == "2")
                {
                    playGame = new GamePlay(new Computer());
                    playGame.p2Name = "Digital Overlord";
                }
                else
                {
                    playGame = new GamePlay(new WeightedComputer());
                    playGame.p2Name = "Librarian";
                    Console.WriteLine("Hint: The Librarian *really* likes paper.");
                }

                playGame.p1Name = player1name;
                Player player1 = new Player();


                //Play game
                GameResult gameOutcome = new GameResult();
                do
                {
                    gameOutcome = playGame.GameLoop(player1, playGame.selector);

                    if (gameOutcome != GameResult.Tie)
                        break;

                    Console.WriteLine("Oh no, it was a tie! Choose again.");

                } while (gameOutcome == GameResult.Tie);

                playGame.EndGame(playGame.p1Choice, playGame.p2Choice, gameOutcome);

                //Ask for rematch
                string wantsRematch = GamePlay.Continue();

                if (wantsRematch.ToLower() == "y")
                {
                    Console.Clear();
                    continue;
                }
                else {
                    Console.WriteLine("Thank You For Playing!");
                    continueGame = false;
                    Console.ReadLine();
                }
                

            } while (continueGame);
        }
    }
}
